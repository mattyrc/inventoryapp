package com.mcates.inventoryapp.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public class InventoryContract {

    // To prevent someone from accidentally instantiating the class.
    private InventoryContract() {
    }

    /**
     * The "Content authority" is a name for the entire content provider.
     */
    public static final String CONTENT_AUTHORITY = "com.mcates.inventoryapp";

    /**
     * The CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
     * the content provider.
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    public static final String PATH_PRODUCTS = "products";

    public static final class InventoryEntry implements BaseColumns {

        /**
         * {@link #CONTENT_URI} for a list of products.
         */
        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCTS;

        /**
         * {@link #CONTENT_URI} for a single product.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PRODUCTS;

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_PRODUCTS);

        // The table name.
        public static final String TABLE_NAME = "products";

        /*
         * Id for the products.
         * Type: INTEGER
         */
        public static final String _ID = BaseColumns._ID;

        /*
         * Name of the product
         * Type: TEXT
         *
         */
        public static final String COLUMN_PRODUCT_NAME = "Product_Name";

        /*
         * Price of the products
         * Type: INTEGER
         */
        public static final String COLUMN_PRICE = "Price";

        /*
         * Quantity of the products
         * Type INTEGER
         */
        public static final String COLUMN_QUANTITY = "Quantity";

        /*
         * Supplier of the product
         * Type: TEXT
         */
        public static final String COLUMN_SUPPLIER_NAME = "Supplier_Name";

        /*
         * Phone number of the distributor.
         * Type: INTEGER
         */
        public static final String COLUMN_SUPPLIER_PHOHE = "Supplier_Phone_Number";
    }
}
