package com.mcates.inventoryapp;

import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mcates.inventoryapp.data.InventoryContract;


public class ViewActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int EXISTING_INVENTORY_LOADER = 0;
    private Uri mCurrentProductUri;

    private TextView mProductNameText;
    private TextView mPriceText;
    private TextView mQuantityText;
    private TextView mSupplierNameText;
    private TextView mSupplierPhoneText;

    private boolean mProductHasChanged = false;


    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            mProductHasChanged = true;
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        mProductNameText = findViewById(R.id.product_name_text);
        mPriceText = findViewById(R.id.price_text);
        mQuantityText = findViewById(R.id.quantity_text);
        mSupplierNameText = findViewById(R.id.name);
        mSupplierPhoneText = findViewById(R.id.phone);

        Intent intent = getIntent();
        mCurrentProductUri = intent.getData();

        if (mCurrentProductUri == null) {
            invalidateOptionsMenu();
        } else {
            getLoaderManager().initLoader(EXISTING_INVENTORY_LOADER, null, this);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
        String[] projection = {
                InventoryContract.InventoryEntry._ID,
                InventoryContract.InventoryEntry.COLUMN_PRODUCT_NAME,
                InventoryContract.InventoryEntry.COLUMN_PRICE,
                InventoryContract.InventoryEntry.COLUMN_QUANTITY,
                InventoryContract.InventoryEntry.COLUMN_SUPPLIER_NAME,
                InventoryContract.InventoryEntry.COLUMN_SUPPLIER_PHOHE
        };
        return new CursorLoader(this, mCurrentProductUri, projection,
                null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        if (cursor.moveToFirst()) {
            int productNameColumnIndex =
                    cursor.getColumnIndex(InventoryContract.InventoryEntry.COLUMN_PRODUCT_NAME);
            int priceColumnIndex =
                    cursor.getColumnIndex(InventoryContract.InventoryEntry.COLUMN_PRICE);
            int quantityColumnIndex =
                    cursor.getColumnIndex(InventoryContract.InventoryEntry.COLUMN_QUANTITY);
            int supplierNameColumnIndex =
                    cursor.getColumnIndex(InventoryContract.InventoryEntry.COLUMN_SUPPLIER_NAME);
            int supplierPhoneColumnIndex =
                    cursor.getColumnIndex(InventoryContract.InventoryEntry.COLUMN_SUPPLIER_PHOHE);

            String productName = cursor.getString(productNameColumnIndex);
            int price = cursor.getInt(priceColumnIndex);
            int quantity = cursor.getInt(quantityColumnIndex);
            String supplierName = cursor.getString(supplierNameColumnIndex);
            final int supplierPhone = cursor.getInt(supplierPhoneColumnIndex);

            mProductNameText.setText(productName);
            mPriceText.setText(Integer.toString(price));
            mQuantityText.setText(Integer.toString(quantity));
            mSupplierNameText.setText(supplierName);
            mSupplierPhoneText.setText(Integer.toString(supplierPhone));
            Button callButton = findViewById(R.id.callButton);
            ImageButton minusButton = findViewById(R.id.subtractBtn);
            ImageButton plusButton = findViewById(R.id.addBtn);

            // Call button that opens the phone intent.
            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String phone = String.valueOf(supplierPhone);
                    Intent intent = new Intent(Intent.ACTION_DIAL,
                            Uri.fromParts("tel", phone, null));
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            });

            // The plus button to increase the inventory count.
            plusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String quantityString = mQuantityText.getText().toString().trim();
                    int quantityInt = Integer.parseInt(quantityString);
                    quantityInt = quantityInt + 1;
                    ContentValues values = new ContentValues();
                    values.put(InventoryContract.InventoryEntry.COLUMN_QUANTITY, quantityInt);
                    getContentResolver().update(mCurrentProductUri, values, null, null);
                }
            });

            // The minus button to decrease the inventory count.
            minusButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String quantityString = mQuantityText.getText().toString().trim();
                    int quantityInt = Integer.parseInt(quantityString);
                    if (quantityInt > 0) {
                        quantityInt = quantityInt - 1;
                        ContentValues values = new ContentValues();
                        values.put(InventoryContract.InventoryEntry.COLUMN_QUANTITY, quantityInt);
                        getContentResolver().update(mCurrentProductUri, values, null, null);
                    } else {
                        Toast.makeText(ViewActivity.this, getText(R.string.sold_out), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            // Floating button to delete the product.
            FloatingActionButton fabDel = (FloatingActionButton) findViewById(R.id.fabDelete);
            fabDel.setBackgroundTintList(getResources().getColorStateList(R.color.colorFabDelete));
            fabDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDeleteConfirmationDialog();
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);

        // Only show edit icon from ViewActivity
        MenuItem item = menu.findItem(R.id.action_edit);
        item.setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Save (check) button in action bar.
            case R.id.action_save:
                saveProduct();
                finish();
                return true;
            // Edit (pencil) button in action bar.
            case R.id.action_edit:
                Intent intent = new Intent(ViewActivity.this, EditorActivity.class);
                intent.setData(mCurrentProductUri);
                startActivity(intent);
                finish();
                return true;
            // Back button in action bar.
            case android.R.id.home:
                if (!mProductHasChanged) {
                    NavUtils.navigateUpFromSameTask(ViewActivity.this);
                    return true;
                }
                // Prompt for discard changes.
                DialogInterface.OnClickListener discardButtonClickListener =
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                NavUtils.navigateUpFromSameTask(ViewActivity.this);
                            }
                        };
                showUnsavedChangesDialog(discardButtonClickListener);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveProduct() {
        String productNameString = mProductNameText.getText().toString().trim();
        String priceString = mPriceText.getText().toString().trim();
        String quantityString = mQuantityText.getText().toString().trim();
        String supplierNameString = mSupplierNameText.getText().toString().trim();
        String supplierPhoneString = mSupplierPhoneText.getText().toString().trim();

        if (mCurrentProductUri == null && TextUtils.isEmpty(productNameString) &&
                TextUtils.isEmpty(priceString) && TextUtils.isEmpty(quantityString) &&
                TextUtils.isEmpty(supplierNameString) && TextUtils.isEmpty(supplierPhoneString)) {
            return;
        }
        ContentValues values = new ContentValues();
        values.put(InventoryContract.InventoryEntry.COLUMN_PRODUCT_NAME, productNameString);
        values.put(InventoryContract.InventoryEntry.COLUMN_PRICE, priceString);
        values.put(InventoryContract.InventoryEntry.COLUMN_QUANTITY, quantityString);
        values.put(InventoryContract.InventoryEntry.COLUMN_SUPPLIER_NAME, supplierNameString);
        values.put(InventoryContract.InventoryEntry.COLUMN_SUPPLIER_PHOHE, supplierPhoneString);

        // Toast messages when adding or updating a product.
        if (mCurrentProductUri == null) {
            // Adding a product.
            Uri newUri =
                    getContentResolver().insert(InventoryContract.InventoryEntry.CONTENT_URI, values);
            if (newUri == null) {
                Toast.makeText(this, getString(R.string.insert_product_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.product_added),
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            // Editing a product.
            int rowsAffected = getContentResolver().update(mCurrentProductUri, values,
                    null, null);
            if (rowsAffected == 0) {
                Toast.makeText(this, getString(R.string.product_update_fail),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.product_update_success),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    // The confirmation prompt for leaving while editing
    private void showUnsavedChangesDialog(
            DialogInterface.OnClickListener discardButtonClickListener) {

        // Create an AlertDialog.Builder and set the message, and click listeners
        // for the positive and negative buttons on the dialog.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.cancel_prompt);
        builder.setPositiveButton(R.string.exit_btn, discardButtonClickListener);
        builder.setNegativeButton(R.string.keep_editing_btn, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                // Cancel if the user clicks "Cancel" button.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    // Prompt the user that progress will be lost.
    private void showDeleteConfirmationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_dialog_msg);
        builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            // If the user chooses to leave, delete product information.
            public void onClick(DialogInterface dialog, int id) {
                deleteProduct();
            }
        });

        // Cancel option.
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked the "Cancel" button, so dismiss the dialog.
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

        // Create and show the AlertDialog
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteProduct() {
        if (mCurrentProductUri != null) {
            int rowsDeleted = getContentResolver().delete(mCurrentProductUri, null, null);
            if (rowsDeleted == 0) {
                Toast.makeText(this, getString(R.string.editor_delete_product_failed),
                        Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.delete_product_success),
                        Toast.LENGTH_SHORT).show();
            }
        }
        finish();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
